for rep in {0..3}; do
	# The .colvars.traj file includes coordinates from rejected exchange moves, which need to be excluded from the histogram.
	# freq is colvarsTrajFrequency
	awk -v freq=5000 'BEGIN {t0=-1;}; !/^#/ && NF>=2 && $1>t0 {print 4e-6*freq*n,$2; n++}; !/^#/ && NF>=2 {t0=$1}' ../../output/rep.R${rep}.0.colvars.traj > rep.R${rep}.traj

	./histoDistro -xmin 2.5 -xmax 11.5 rep.R${rep}.traj 80 hist_rep.R${rep}.traj

	kB=0.001987191
	awk -v kB=$kB '$2>0 {print $1,-kB*285*log($2)}' hist_rep.R${rep}.traj > tmp.dat
    
    #./anchorEnergy.sh tmp.dat 2.5 2.825 rep.R${rep}.pmf
	./anchorEnergy.sh tmp.dat 2.775 2.825 rep.R${rep}.pmf
done
