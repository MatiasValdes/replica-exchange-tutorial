#!/bin/bash

#if (( $# < 4 )); then
#    echo "Usage: bash anchorPmf.sh inPmf x0 x1 outPmf"
#    exit -1
#fi
f=$1
x0=$2
x1=$3
out=$4

mean=$( awk -v x0=$x0 -v x1=$x1 '!/^#/ && NF>=2 && $1>=x0 && $1<=x1 {s+=$2; n++}; END {print s/n}' $1 )
awk -v mean=$mean '!/^#/ && NF>=2 {print $1,$2-mean}' $f > $out
