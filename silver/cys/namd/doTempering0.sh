run=0
namdLoc=$HOME/Descargas/NAMD_2.13_Linux-x86_64-multicore/namd2

name=rep
#numReplicas=10
#numReplicas=6
numReplicas=4
procs=4

for ((rep=0; rep<numReplicas; rep++)); do
    out=$name.ex${rep}.$run.namd
    :>$out

    echo "set replica $rep" >> $out
    echo "set solTemperMin 285.0" >> $out
    echo "set solTemperMax 550.0" >> $out
    echo "set numReplicas $numReplicas" >> $out
    echo "set cycles 20000" >> $out
    echo "set cycleSteps 50000" >> $out
    echo "" >> $out
    echo "set run $run" >> $out
    echo "set name $name" >> $out
    echo "set temper 285" >> $out
    echo "set struct agWat_cys_final" >> $out
    echo "set initName ../eq.agWat_cys_final.1.restart" >> $out
    # echo "set soluteFile ../solute_threeK_unfold_sol.pdb" >> $out
    echo "set colvarsFile ../1d_disZ_agWat_cys.colvars" >> $out
    echo "source ../template_replica.namd" >> $out
    
    $namdLoc +p${procs} $out > ${out%.*}.log &
    #$namdLoc +idlepoll +p${procs} +devices 0 $out > ${out%.*}.log &

done
